name := "Project2"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies += "org.apache.kafka" %% "kafka" % "2.1.0"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"