import java.time.Duration
import java.util.Properties

import Main.i

import scala.util.Random
import scala.collection.JavaConverters._
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord, ConsumerRecords, KafkaConsumer}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}

object Main extends App {
  val props: Properties = new Properties()
  props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
  props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer])
  props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer])

  val producer: KafkaProducer[String, String] = new KafkaProducer[String, String](props)

  var array = Array("Tuileries", "Concorde", "Champs Elysées Clemenceau", "Montparnasse",
  "Franklin D. Roosevelt", "George V", "Charles de Gaulle Etoile", "Argentine",
  "Louvre-Rivoli", "Ternes", "Saint-Paul", "Bastille", "Victor Hugo",
  "Saint-Michel", "Opéra", "Madeleine", "Invalides", "Porte de Versailles",
  "Magenta", "Auber", "Solférino", "Musée d'Orsay")
  var arrayLenght = array.length

  var i = 0
  val randomNumber = new Random()
  while(i < 100)
  {
    for(id <- 1 until 101) {
      var randomValue = randomNumber.nextInt(15)
      var newEvent = 0
      randomValue match {
        case 0 => {
          val record = new ProducerRecord[String, String]("problem", s"${id}", "Bus is out of fuel")
          producer.send(record)
        }
        case 1 => {
          val record = new ProducerRecord[String, String]("problem", s"${id}", "Bus is full")
          producer.send(record)
        }
        case 2 => {
          val record = new ProducerRecord[String, String]("problem", s"${id}", "Bus hit something")
          producer.send(record)
        }
        case 3 => {
          val record = new ProducerRecord[String, String]("problem", s"${id}", "The bus had an accident")
          producer.send(record)
        }
        case 4 => {
          val record = new ProducerRecord[String, String]("topic", s"${id}", "Bus stuck into traffic")
          producer.send(record)
        }
        case 5 => {
          val record = new ProducerRecord[String, String]("problem", s"${id}", "The bus' tire punctered.")
          producer.send(record)
        }
        case 6 => {
          val record = new ProducerRecord[String, String]("problem", s"${id}", "A problem occured with the doors")
          producer.send(record)
        }
        case _ => {
          println("Nothing happens")
        }
      }
      randomValue = randomNumber.nextInt(arrayLenght)
      val numberPassengerOut = randomNumber.nextInt(50)
      val numberPassengerIn = randomNumber.nextInt(50)
      println(s"Station = ${array(randomValue)}")
      val record = new ProducerRecord[String, String]("station", s"${id}", s"${array(randomValue)},${numberPassengerIn},${numberPassengerOut}")
      producer.send(record)
    }
    Thread.sleep(10000)
    i = i + 1
  }

  producer.close()
}