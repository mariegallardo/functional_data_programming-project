package utils

import com.google.gson._

object LocalizationUtils {
  case class Localization (
                      busId : String,
                      station : String,
                      passengerIn : String,
                      passengerOut : String
                    )


  def parseFromJsonLoca(lines:Iterator[String]):Iterator[Localization] = {
    val gson = new Gson
    lines.map(line => gson.fromJson(line, classOf[Localization]))
  }
}
