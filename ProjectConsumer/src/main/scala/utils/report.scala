package utils

import com.google.gson._

object ReportUtils {
  case class Report (
                     busId : String,
                     message : String
                   )


  def parseFromJson(lines:Iterator[String]):Iterator[Report] = {
    val gson = new Gson
    lines.map(line => gson.fromJson(line, classOf[Report]))
  }
}
