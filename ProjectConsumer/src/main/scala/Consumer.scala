import java.time.Duration
import java.util.Properties

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import java.io.FileWriter

import scala.collection.JavaConverters._
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord, ConsumerRecords, KafkaConsumer}
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.apache.spark.rdd.RDD
import utils.ReportUtils._
import utils.LocalizationUtils._

object Main extends App {

  def loadData(pathToFile: String): RDD[Report] = {
    // Create the spark configuration and spark context
    val conf = new SparkConf()
      .setAppName("User mining")
      .setMaster("local[*]")

    val sc = SparkContext.getOrCreate(conf)

    sc.textFile(pathToFile).mapPartitions(parseFromJson(_))
  }

  def loadDataLoca(pathToFile: String): RDD[Localization] = {
    // Create the spark configuration and spark context
    val conf = new SparkConf()
      .setAppName("User mining")
      .setMaster("local[*]")

    val sc = SparkContext.getOrCreate(conf)

    sc.textFile(pathToFile).mapPartitions(parseFromJsonLoca(_))
  }

  // return the reports of each bus
  def reportsByBus(): RDD[(String, Iterable[Report])] = {
    loadData("src/main/resources/report.json")
      .groupBy(report => report.busId)
  }

  // return the number of reports of each type
  def reportsByType(): RDD[(String, Int)] = {
    loadData("src/main/resources/report.json")
      .map(r => (r.message, 1))
      .reduceByKey((acc, i) => acc + i)
  }

  // return the 10 bus with the most problems
  def mostProblematicBus(): Array[(String, Int)] = {
    loadData("src/main/resources/report.json")
      .map(report => (report.busId, 1))
      .reduceByKey((acc, i) => acc + i)
      .sortBy(-_._2)
      .take(10)
  }

  // returns the percentage of occurence of a problem
  def percentageOfProblem(problemDescription: String): Float = {
    reportsByType().filter(report => report._1 == problemDescription).first()._2.floatValue() / (loadData("src/main/resources/report.json").count())
  }

  // returns the 10 first station which have the biggest difference between the passengers that enter the bus and the ones that get off.
  def stationMostInPassengers(): Array[(String, Int)] = {
    loadDataLoca("src/main/resources/localization.json")
      .map(localization => (localization.station, Integer.parseInt(localization.passengerIn) - Integer.parseInt(localization.passengerOut)))
      .reduceByKey((acc, i) => acc + i)
      .sortBy(-_._2)
      .take(10)
  }


  val props2: Properties = new Properties()

  props2.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
  props2.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer])
  props2.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer])
  props2.put(ConsumerConfig.GROUP_ID_CONFIG, "myconsumergroup")
  //props2.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")

  val consumer: KafkaConsumer[String, String] = new KafkaConsumer[String, String](props2)
  consumer.subscribe(List("station", "problem").asJava)
  println("Polling records from kafka and writing them in json  ")

  var filepath = ""
  var i = 0
  while(i <= 5) {
    val records: ConsumerRecords[String, String] = consumer.poll(Duration.ofMillis(1000))
    records.asScala.foreach { record =>
      println(s"offset = ${record.offset()}, key = ${record.key()}, value = ${record.value()}")
      // sort the records depending on the topic: station goes in localization.json, problem goes in report.json
      if (record.topic() == "station") {
        // there is several different messages seperated by a coma in this topic, so we are splitting the message:
        val messageContents = record.value().split(",")
        filepath = "src/main/resources/localization.json"
        val fw = new FileWriter(filepath, true)
        fw.write("{\"busId\":\"" + record.key() + "\",\"station\":\"" + messageContents(0) + "\",\"passengerIn\":\"" + messageContents(1) +"\",\"passengerOut\":\"" + messageContents(2) + "\"}\n")
        fw.close()
      }
      else {
        filepath = "src/main/resources/report.json"
        val fw = new FileWriter(filepath, true)
        fw.write("{\"busId\":\"" + record.key() + "\",\"message\":\"" + record.value() + "\"}\n")
        fw.close()
      }
    }
    i = i + 1
  }


  println("Some analysis:")
  println("------------------------------------------------")
  println("Bus reports sorted by bus:")
  reportsByBus().foreach(x => println(x))
  println("------------------------------------------------")
  println("Number of each reports:")
  println("(type_of_report, Number_of_said_report)")
  reportsByType().foreach(x => println(x))
  println("------------------------------------------------")
  println("top 10 most problematic bus:")
  println("(number_of_the_bus, number_of_report_concerning_the_bus)")
  mostProblematicBus().foreach(x => println(x))
  println("------------------------------------------------")
  println("Bus is out of fuel percentage :" + percentageOfProblem("Bus is out of fuel"))
  println("------------------------------------------------")
  println("Station with the most people entering while having little people get off:")
  println("(number_of_the_station, total_delta_between_passengerIn_and_passengerOut)")
  stationMostInPassengers().foreach(x => println(x))
}