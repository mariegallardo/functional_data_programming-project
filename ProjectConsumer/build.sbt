
name := "ProjectConsumer"

version := "0.1"

scalaVersion := "2.12.8"


libraryDependencies += "org.apache.kafka" %% "kafka" % "2.1.0"

/*libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"*/

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.4.1"

dependencyOverrides ++= Seq(
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.6.7.2"
)

fork in run :=true

/*libraryDependencies ++= Seq(
  "org.apache.spark" % "spark-core_2.11" % "2.2.0",
  "org.apache.spark" % "spark-sql_2.11" % "2.2.0",
  "org.apache.spark" % "spark-mllib_2.10" % "1.1.0"
)*/
/*libraryDependencies += "org.apache.spark" %% "spark-core" % "2.3.0"*/