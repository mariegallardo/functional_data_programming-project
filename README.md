This is our Functional data programming project.
Our objective is to monitor the proper functioning of the buses' system and
analyse this data.

I Architecture.

There is 2 intellij Idea projects: the producer and the consumer.
The producer project produce messages in kafka while the consumer consumes them
and manage everything related to Spark. 
The producer put messages in kafka every 10 seconds to simulated the time. There
are 2 topics: 

-station: to know where the buses have arrived, how people got in and off.

-problem: to know every problem that has occured.

The consumer put the messages received in two .json files depending on the topic
it comes from: the messages from topic "problem" are put in report.json and 
those from "station" are put in localization.json. The consumer also launch
different analysis on the data created.

II How to launch the project:

-launch zookeeper and kafka

-open and launch the producer project with Run -> Run the program

-open and launch the consumer project with Run -> Run the program

-results should be displayed in the sbt shell



Marie GALLARDO
Nicolas PHILIPPE